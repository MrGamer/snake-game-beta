// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

#include "ActorSnake.h"
#include "WorldGenerator.h"

#include "MainPawn.generated.h"


UCLASS()
class SNAKE_API AMainPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMainPawn();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Component")
		class UCameraComponent* Camera;

	UPROPERTY(BlueprintReadOnly)
		AActorSnake* SnakeActor;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		TSubclassOf<AActorSnake> SnakeActorClass;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);

	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
};
