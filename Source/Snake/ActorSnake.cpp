// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorSnake.h"

// Sets default values
AActorSnake::AActorSnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	MovementSpeed = 200.f;
	ElementSize = 70.f;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void AActorSnake::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3, ElementSize);
	SetActorTickInterval(MovementSpeed); ///��� ������ ���� ������� ����� ������ ������� ����, � ������������� � �������� ���������� �����
	
	
}

// Called every frame
void AActorSnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void AActorSnake::AddSnakeElement(int ElemCount, float ElSize)
{

	for (int i = 0; i < ElemCount; i++)
	{
		FVector NewLocation(FVector((massSize / 2) * ElSize - SnakeElements.Num() * ElSize, (massSize - 1) * ElSize, 0));
		FTransform NewTransform(NewLocation);
		if (!isStarted) {
			
			NewTransform= FTransform(NewLocation);
		}
		else {

			NewTransform = FTransform(PrevLockation);
		}
		AActorTail* NewSnakeElement = GetWorld()->SpawnActor<AActorTail>(SnakeTailClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);

		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();

		}
		
	}

}

void AActorSnake::Move()
{
	
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {

		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		PrevLockation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLockation);
	}


	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	isStarted = true;
}

void AActorSnake::SnakeElementOverlap(AActorTail* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IiInteractable* InteractableInterface = Cast<IiInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void AActorSnake::SpeedUp()
{
	MovementSpeed *= 0.95;
	SetActorTickInterval(MovementSpeed);
}

void AActorSnake::SnakeDestroyer()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
}


