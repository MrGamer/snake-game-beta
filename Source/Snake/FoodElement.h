// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "iInteractable.h"
#include "ActorSnake.h"


#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "FoodElement.generated.h"

class AFood;

UCLASS()
class SNAKE_API AFoodElement : public AActor, public IiInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodElement();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	//UPROPERTY(EditDefaultsOnly)
		//TSubclassOf<AFood> FoodMainClass;
	UPROPERTY()
		AFood* FoodMain;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
