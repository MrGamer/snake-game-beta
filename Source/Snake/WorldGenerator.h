// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Obstacles.h"
#include "Floor.h"
#include "Components/StaticMeshComponent.h"

#include "Math/UnrealMathUtility.h"
#include "WorldGenerator.generated.h"

#define massSize 16

UCLASS()
class SNAKE_API AWorldGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldGenerator();

	UPROPERTY()
		float ObstacleSize = 60.f;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AObstacles> ObstacleClass;

	UPROPERTY()
			TArray<AObstacles*> ObtacleElement;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFloor> FloorClass;

	UPROPERTY()
		TArray<AFloor*> FloorElement;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void PlaceObstacleElement(int32 x, int32 y, float z, float ObstSize);

	void PlaceFloorElement(int32 x, int32 y, float z, float ObstSize);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void GeneratorField();

};
