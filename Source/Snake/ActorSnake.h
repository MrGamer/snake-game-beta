// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "iInteractable.h"
#include "ActorTail.h"
#include "WorldGenerator.h"


#include "Components/StaticMeshComponent.h"
#include "ActorSnake.generated.h"


UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API AActorSnake : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorSnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActorTail> SnakeTailClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		TArray<AActorTail*> SnakeElements; //������ ���������� �� �������� ������

	UPROPERTY()
		EMovementDirection LastMoveDirection;
	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY()
		bool isStarted;
	UPROPERTY()
	FVector PrevLockation;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElemCount = 1, float ElSize = 50.f);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(AActorTail* OverlappedElement, AActor* Other);

	UFUNCTION()
		void SpeedUp();

	UFUNCTION()
		void SnakeDestroyer();
		
};
