// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodElement.h"
#include "Food.h"

// Sets default values
AFoodElement::AFoodElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFoodElement::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFoodElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodElement::Interact(AActor* Interactor, bool bIsHead)

	{
		if (bIsHead)
		{
			auto Snake = Cast<AActorSnake>(Interactor);

			if (IsValid(Snake))
			{
				Snake->AddSnakeElement();
				Snake->SpeedUp();
				if (IsValid(FoodMain))
				{
					FoodMain->CreateFood();
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("Nope"));
				}
				Destroy();
							
			}
			
		}
	}



