// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/iInteractable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeiInteractable() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_UiInteractable_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UiInteractable();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Snake();
// End Cross Module References
	void UiInteractable::StaticRegisterNativesUiInteractable()
	{
	}
	UClass* Z_Construct_UClass_UiInteractable_NoRegister()
	{
		return UiInteractable::StaticClass();
	}
	struct Z_Construct_UClass_UiInteractable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UiInteractable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UiInteractable_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "iInteractable.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UiInteractable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IiInteractable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UiInteractable_Statics::ClassParams = {
		&UiInteractable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UiInteractable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UiInteractable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UiInteractable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UiInteractable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UiInteractable, 1577129288);
	template<> SNAKE_API UClass* StaticClass<UiInteractable>()
	{
		return UiInteractable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UiInteractable(Z_Construct_UClass_UiInteractable, &UiInteractable::StaticClass, TEXT("/Script/Snake"), TEXT("UiInteractable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UiInteractable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
