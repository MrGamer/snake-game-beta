// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/Obstacles.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeObstacles() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_AObstacles_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AObstacles();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UiInteractable_NoRegister();
// End Cross Module References
	void AObstacles::StaticRegisterNativesAObstacles()
	{
	}
	UClass* Z_Construct_UClass_AObstacles_NoRegister()
	{
		return AObstacles::StaticClass();
	}
	struct Z_Construct_UClass_AObstacles_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Obstacles_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Obstacles_StaticMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AObstacles_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObstacles_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Obstacles.h" },
		{ "ModuleRelativePath", "Obstacles.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AObstacles_Statics::NewProp_Obstacles_StaticMesh_MetaData[] = {
		{ "Category", "Component" },
		{ "Comment", "//???? ???????????\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Obstacles.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AObstacles_Statics::NewProp_Obstacles_StaticMesh = { "Obstacles_StaticMesh", nullptr, (EPropertyFlags)0x00100000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AObstacles, Obstacles_StaticMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AObstacles_Statics::NewProp_Obstacles_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AObstacles_Statics::NewProp_Obstacles_StaticMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AObstacles_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AObstacles_Statics::NewProp_Obstacles_StaticMesh,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AObstacles_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UiInteractable_NoRegister, (int32)VTABLE_OFFSET(AObstacles, IiInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AObstacles_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AObstacles>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AObstacles_Statics::ClassParams = {
		&AObstacles::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AObstacles_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AObstacles_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AObstacles_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AObstacles_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AObstacles()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AObstacles_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AObstacles, 467367918);
	template<> SNAKE_API UClass* StaticClass<AObstacles>()
	{
		return AObstacles::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AObstacles(Z_Construct_UClass_AObstacles, &AObstacles::StaticClass, TEXT("/Script/Snake"), TEXT("AObstacles"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AObstacles);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
