// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_iInteractable_generated_h
#error "iInteractable.generated.h already included, missing '#pragma once' in iInteractable.h"
#endif
#define SNAKE_iInteractable_generated_h

#define Snake_Source_Snake_iInteractable_h_13_SPARSE_DATA
#define Snake_Source_Snake_iInteractable_h_13_RPC_WRAPPERS
#define Snake_Source_Snake_iInteractable_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_iInteractable_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_API UiInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiInteractable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_API, UiInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiInteractable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_API UiInteractable(UiInteractable&&); \
	SNAKE_API UiInteractable(const UiInteractable&); \
public:


#define Snake_Source_Snake_iInteractable_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_API UiInteractable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_API UiInteractable(UiInteractable&&); \
	SNAKE_API UiInteractable(const UiInteractable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_API, UiInteractable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UiInteractable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UiInteractable)


#define Snake_Source_Snake_iInteractable_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUiInteractable(); \
	friend struct Z_Construct_UClass_UiInteractable_Statics; \
public: \
	DECLARE_CLASS(UiInteractable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Snake"), SNAKE_API) \
	DECLARE_SERIALIZER(UiInteractable)


#define Snake_Source_Snake_iInteractable_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_Source_Snake_iInteractable_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_Source_Snake_iInteractable_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_iInteractable_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_Source_Snake_iInteractable_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_Source_Snake_iInteractable_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_iInteractable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IiInteractable() {} \
public: \
	typedef UiInteractable UClassType; \
	typedef IiInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_Source_Snake_iInteractable_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IiInteractable() {} \
public: \
	typedef UiInteractable UClassType; \
	typedef IiInteractable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_Source_Snake_iInteractable_h_10_PROLOG
#define Snake_Source_Snake_iInteractable_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_iInteractable_h_13_SPARSE_DATA \
	Snake_Source_Snake_iInteractable_h_13_RPC_WRAPPERS \
	Snake_Source_Snake_iInteractable_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_iInteractable_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_iInteractable_h_13_SPARSE_DATA \
	Snake_Source_Snake_iInteractable_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_iInteractable_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class UiInteractable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_iInteractable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
