// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_Obstacles_generated_h
#error "Obstacles.generated.h already included, missing '#pragma once' in Obstacles.h"
#endif
#define SNAKE_Obstacles_generated_h

#define Snake_Source_Snake_Obstacles_h_17_SPARSE_DATA
#define Snake_Source_Snake_Obstacles_h_17_RPC_WRAPPERS
#define Snake_Source_Snake_Obstacles_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_Obstacles_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObstacles(); \
	friend struct Z_Construct_UClass_AObstacles_Statics; \
public: \
	DECLARE_CLASS(AObstacles, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AObstacles) \
	virtual UObject* _getUObject() const override { return const_cast<AObstacles*>(this); }


#define Snake_Source_Snake_Obstacles_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAObstacles(); \
	friend struct Z_Construct_UClass_AObstacles_Statics; \
public: \
	DECLARE_CLASS(AObstacles, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AObstacles) \
	virtual UObject* _getUObject() const override { return const_cast<AObstacles*>(this); }


#define Snake_Source_Snake_Obstacles_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObstacles(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObstacles) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacles); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacles); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacles(AObstacles&&); \
	NO_API AObstacles(const AObstacles&); \
public:


#define Snake_Source_Snake_Obstacles_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacles(AObstacles&&); \
	NO_API AObstacles(const AObstacles&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacles); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacles); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AObstacles)


#define Snake_Source_Snake_Obstacles_h_17_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_Obstacles_h_14_PROLOG
#define Snake_Source_Snake_Obstacles_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Obstacles_h_17_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Obstacles_h_17_SPARSE_DATA \
	Snake_Source_Snake_Obstacles_h_17_RPC_WRAPPERS \
	Snake_Source_Snake_Obstacles_h_17_INCLASS \
	Snake_Source_Snake_Obstacles_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_Obstacles_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_Obstacles_h_17_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_Obstacles_h_17_SPARSE_DATA \
	Snake_Source_Snake_Obstacles_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_Obstacles_h_17_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_Obstacles_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AObstacles>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_Obstacles_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
